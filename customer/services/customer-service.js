const BASE_URL = `https://640c9f16a3e07380e8f90b3d.mockapi.io`;

export let getListProduct = () => {
  return axios({
    url: `${BASE_URL}/phone`,
    method: 'GET',
  });
};

export let getProductByID = (id) => {
  return axios({
    url: `${BASE_URL}/phone/${id}`,
    method: 'GET',
  });
};
