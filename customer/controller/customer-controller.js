import { CartItem1, CartItem2 } from '../model/customer-model.js';

export let renderPhoneListProduct = (phoneArr) => {
  let phoneList = phoneArr.map((item) => {
    return `
<div class="col-lg-3 col-md-4 mr-auto mt-5">
    <div class="card text-black h-100">
    <div class="content-overlay w-100"></div>
      <img src='${item.img}' class="card-img" alt="Phone Image">
      <div class="content-details fadeIn-top">
      <h3 class="pb-5">Specifications</h3>
            <div class="d-flex justify-content-start py-1">
          <span class="text-light"><b>Screen:</b></span>
          <span class="text-light">&nbsp; ${item.screen}</span>
        </div>
        <div class="d-flex justify-content-start py-1">
          <span class="text-light"><b>Back Camera:</b> ${item.backCamera}</span>
        </div>
        <div class="d-flex justify-content-start py-1">
          <span class="text-light"><b>Front Camera:</b> ${
            item.frontCamera
          }</span>
        </div>

        <p class="pt-5" onClick='clickToVewDetail(res.data)'><u>click here for more details</u></p>
      </div>
      <div class="card-body">
        <div class="text-center">
          <h5 class="card-title pt-3">${item.name}</h5>
          <span class="text-muted mb-2">${currency(item.price, {
            symbol: '$',
          }).format()}</span>
          <span class="text-danger"><s>$500</s></span>
        </div>
        <div class="mt-3 brand-box text-center">
          <span>${item.type}</span>
        </div>
        <div class="d-flex justify-content-start pt-3">
          <span><b>Description:</b> ${item.desc}</span>
        </div>
        <div class="d-flex justify-content-between pt-3">
          <div class="text-warning">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
          </div>
          <span class="text-success"><b>In Stock</b></span>
        </div>
        <button type="button" class="btn btn-block w-50" onclick="addToCart(${
          item.id
        })">Add to cart</button>
      </div>
    </div>
  </div>
`;
  });

  document.getElementById('phoneList').innerHTML = phoneList;
};
// filter brand
export let filter = (itemList) => {
  let selectOption = document.getElementById('selectList');
  if (selectOption.value == 'all') {
    renderPhoneListProduct(itemList);
  } else {
    let result = itemList.filter((item) => {
      return item.type === selectOption.value;
    });
    renderPhoneListProduct(result);
  }
};

// on loading
export let onLoading = () => {
  document.querySelector('.loading').style.display = 'flex';
};

// off loading
export let offLoading = () => {
  document.querySelector('.loading').style.display = 'none';
};

// render cart list

//itemTotal
let itemTotal = (price, quantity) => {
  return price * quantity;
};
//
export let renderShoppingCart = (listCartItem) => {
  let total = 0;
  let cartCount = 0;
  let cartItem = listCartItem.map((item, index) => {
    total += itemTotal(item.product.price, item.quantity);
    cartCount += item.quantity;
    return `<tr class = "py-2 text-center"><td>
            <img
              class="img_giohang"
              src="${item.product.img}"
            /></td>
            <td>${item.product.name}</td>
            <td class="quanty">
              <button onclick = "decreaseItemAmount(${index})" class "btn btn-warning"><i class="fa fa-minus"></i></button>
              <span class = "Quantity">${item.quantity}</span>
              <button onclick ="increaseItemAmout(${index})" class "btn btn-warning"><i class="fa fa-plus"></i></button>
            </td>
            <td>$ ${item.product.price}</td>
            <td>${itemTotal(item.product.price, item.quantity)}</td>
            <td><button onclick = "removeItem(${index})"><i class="fa fa-trash"></i></i></button></td>
            </tr>
            `;
  });
  document.getElementById('cart_item').innerHTML = cartItem;
  document.getElementById('total').innerHTML = total;
  document.getElementById('cartCount').innerHTML = cartCount;
};
